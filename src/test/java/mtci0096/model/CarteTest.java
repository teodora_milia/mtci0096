package mtci0096.model;

import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class CarteTest {
    Carte c;
    Carte c1;
    Carte carte;
    private List<String> autoriT = new ArrayList<>(
            Arrays.asList("Mircea Eliade", "Camil Petrescu"));


    //se apeleaza o sg data inaintea oricarui test
    @BeforeClass
    public static void setup(){
        System.out.println("Before any test");
    }

    @AfterClass
    public static void setupAfter(){
        System.out.println("After all tests");
    }
    //se apeleaza doar inaintea testului asupra caruia ii este adnotat
    @Before
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("povesti");
        c.setEditura("Litera");
        c.setAnAparitie("1948");
        c.adaugaAutor("Nichita Stenascu");
        c.adaugaAutor("Mihai Eminescu");
        System.out.println("in BeforeTest");
    }

    @After
    public void tearDown() throws Exception {
        c = null;
        c1 = null;
        carte = null;
        System.out.println("in AfterTest");
    }

    @Ignore //ignora testul asupra caruia este pus
    @Test
    public void getTitlu() {
        assertEquals("titlu = povesti", "povesti",c.getTitlu());
        // assertTrue("povesti", c.getTitlu());
    }
    @Test
    public void setTitlu() {
        c.setTitlu("fabule");
        assertEquals("fabule",c.getTitlu());
        // assertTrue("povesti", c.getTitlu());
    }


    @Test(timeout = 1000)
    public void getEditura(){
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
            assertEquals(c.getEditura(), "Litera");
        }

    }
    //aici testul trece daca mi se arunca exceptia. trb sa simulam un comportament care sa arunce exceptia
    //daca pun an mai mic ca 1900 testu trece pt ca imi merge in exceptie (adica are de aruncat exceptie)
    @Test(expected = Exception.class)
    public void SetAnAparitie() throws Exception {
        c.setAnAparitie("1899");
    }
    @Ignore //ignora testul asupra caruia este pus
    @Test
    public void getAnAparitie() {
        assertEquals("anAparitie = 1948", "1948",c.getAnAparitie());
    }

    public void testConstructor(){
        Carte c3 = new Carte();
        assertEquals(c3, null);
    }

    @Test(timeout = 200)
    public void getAnAparitie2() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
            assertEquals(c.getAnAparitie(), "1948");
        }
    }
   @Test
    public void cautaDupaAutor() {
        c.cautaDupaAutor("Nichita Stenascu");
        assertEquals(false,  c.cautaDupaAutor("Nichita Stanescu"));
    }
    @Test
    public void cautaDupaAutor2() {
        //c.cautaDupaAutor("Mihai Eminescu");
        assertEquals(true,  c.cautaDupaAutor("Mihai Eminescu"));
    }
    @Test(timeout = 200)
    public void getAutori() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
            List<String> autoriT=new ArrayList<>(
                    Arrays.asList("Mircea Eliade", "Victor Ionescu"));
            assertTrue(autoriT.equals(autoriT));
        }
    }


}